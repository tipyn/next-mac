export bdoLastDir=$(pwd)

cp ~/.bash_profile ~/repos/personal/next-mac/dotfiles
cp ~/Library/Application\ Support/Code/User/settings.json ~/repos/personal/next-mac/dotfiles/vscode_settings.json
cp ~/.cvimrc ~/repos/personal/next-mac/dotfiles
cp ~/.gitignore ~/repos/personal/next-mac/dotfiles
cp ~/.zshrc ~/repos/personal/next-mac/dotfiles
cp -r ~/.oh-my-zsh/* ~/repos/personal/next-mac/dotfiles/oh-my-zsh/
cp ~/repos/workspaces* ~/repos/personal/next-mac/workspaces

cd ~/repos/personal/next-mac/dotfiles
#git stash
git checkout master
git add ./.bash_profile
git add ./vscode_settings.json
git add ./.cvimrc
git add ./.zshrc
git add ./oh-my-zsh
cd ./oh-my-zsh
git add .
git commit -m "Auto-save from bash ($(date +"%F"))"
git push

cd $bdoLastDir
