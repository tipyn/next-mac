alias finder='ofd'
alias backup='/Users/brendan/repos/personal/next-mac/dotfiles/auto-save-bash.sh'

alias reset='clear && source ~/.zshrc'

alias upstream='ggsup'
alias setupstream='ggsup'

export GITLAB_API_ENDPOINT=https://gitlab.com/api/v4
export GITLAB_API_PRIVATE_TOKEN=$(cat ~/.gitlab-token)

cat ~/gitlab_words

###
# GitLab `lab` CLI section
###

alias ci='lab ci view'
alias traceci='lab ci trace'
alias lint='lab ci lint'

###
# GitLab repo stuff
###
alias testwww='bundle exec rake lint'

###
# Brendan's CLI todo-er
###

alias todo='cd ~/repos/personal/todo && lab issue list && cd -'
