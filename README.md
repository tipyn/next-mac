# First Installs

* [Chrome]
* [Slack]
* [Homebrew]
* [Visual Studio Code]
* [Caffeine](http://lightheadsw.com/caffeine/)
* [Dropbox]

# Terminal Setup

* Oh my zsh
1. Install oh-my-zsh
1. Reload oh-my-zsh folder from here
1. Reload .zshrc from here
1. Run `sudo easy_install Pygments`

* Brew installs

```
brew install gpg git redis postgresql libiconv icu4c pkg-config cmake nodejs go openssl node npm yarn coreutils re2
brew install gnupg gnupg2
```

* NPM Globals

npm install -g yo generator-code

* Git configs

git config --global push.default current

* Color & Style
Install `homebrew.terminal`

* Variables
<pre>
JAVA_HOME=/System/Library/Java/JavaVirtualMachines/1.8.0.jdk/Contents/Home
export JAVA_HOME
export PATH=$PATH:$JAVA_HOME/binexport JAVA_HOME="$(/usr/libexec/java_home -v 1.8)"
export PATH=/Users/Brendan/Applications/terraform/:$PATH
alias ll='ls -lG'
alias meteors='meteor --settings settings.json'
source ~/.profile

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/Brendan/Applications/google-cloud-sdk/path.bash.inc' ]; then source '/Users/Brendan/Applications/google-cloud-sdk/pat$

# The next line enables shell command completion for gcloud.
if [ -f '/Users/Brendan/Applications/google-cloud-sdk/completion.bash.inc' ]; then source '/Users/Brendan/Applications/google-cloud-s$
</pre>

# Other Apps

* [Docker]
* [F.lux]
* [Skype]
* [Zoom]
* [Insomina](https://insomnia.rest/)
* [Battle.net](http://battle.net)
* [Discord](https://discordapp.com/)
* [Minecraft]

# Maybes

## Randoms

* [GIPHY Capture]
* [Bartender 3]

## Programing

* [XCode]
* [WebStorm]
* [RVM]

RVM itself:

   `\curl -sSL https://get.rvm.io | bash`

Readlines support:

   `rvm pkg install readline --verify-downloads 1`
   `ruby_configure_flags=--with-readline-dir="$rvm_path/usr"`
   `rvm reinstall 2.3.1`
