#!/bin/bash

# Turn on Do Not Disturb until tomorrow
defaults -currentHost write ~/Library/Preferences/ByHost/com.apple.notificationcenterui doNotDisturb -boolean true
defaults -currentHost write ~/Library/Preferences/ByHost/com.apple.notificationcenterui doNotDisturbDate -date "`date -u +\"%Y-%m-%d %H:%M:%S +000\"`"
killall NotificationCenter

# Kill apps by name
pkill -9 "iTunes"
pkill -9 "Slack"
pkill -9 "Message"
pkill -9 "Station"
pkill -9 "Chrome"

# Caffinate
caffeinate -s -t 10800 &

exit 0